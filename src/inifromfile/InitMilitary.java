package inifromfile;

import data.AbstractAircraft;
import data.AttackPlane;
import data.Bomber;
import data.Fighter;

import java.util.List;

import static utils.Common.isBomber;
import static utils.Common.isFighter;

/**
 * Created by Andoliny on 17.10.2015.
 */
public class InitMilitary extends ConstructorChooser {


    public AbstractAircraft createAircraft(List<String> paramsList) {

        if (isBomber(paramsList.get(0))) {
            return new Bomber(paramsList.get(1), paramsList.get(2), Double.parseDouble(paramsList.get(3)), Double.parseDouble(paramsList.get(4)), Double.parseDouble(paramsList.get(5)), Integer.parseInt(paramsList.get(6)), Integer.parseInt(paramsList.get(7)), Double.parseDouble(paramsList.get(8)), Integer.parseInt(paramsList.get(9)), Double.parseDouble(paramsList.get(10)));
        } else if (isFighter(paramsList.get(0))) {
            return new Fighter(paramsList.get(1), paramsList.get(2), Double.parseDouble(paramsList.get(3)), Double.parseDouble(paramsList.get(4)), Double.parseDouble(paramsList.get(5)), Integer.parseInt(paramsList.get(6)), Integer.parseInt(paramsList.get(7)), Double.parseDouble(paramsList.get(8)), Integer.parseInt(paramsList.get(9)), Boolean.parseBoolean(paramsList.get(10)));
        } else
            return new AttackPlane(paramsList.get(1), paramsList.get(2), Double.parseDouble(paramsList.get(3)), Double.parseDouble(paramsList.get(4)), Double.parseDouble(paramsList.get(5)), Integer.parseInt(paramsList.get(6)), Integer.parseInt(paramsList.get(7)), Double.parseDouble(paramsList.get(8)), Integer.parseInt(paramsList.get(9)), Boolean.parseBoolean(paramsList.get(10)));
    }
}
