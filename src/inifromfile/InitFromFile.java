package inifromfile;

import data.AbstractAircraft;

/**
 * Created by Andoliny on 17.10.2015.
 */
public interface InitFromFile {

    public AbstractAircraft initAircraft(String s);
}
