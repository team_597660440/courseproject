package inifromfile;

import data.AbstractAircraft;
import data.PassengerAircraft;
import data.SportAircraft;
import data.TransportAircraft;

import java.util.List;

import static utils.Common.isPassenger;
import static utils.Common.isSport;

/**
 * Created by Andoliny on 18.10.2015.
 */
public class InitCivil extends ConstructorChooser {

    public AbstractAircraft createAircraft(List<String> paramsList) {

        if(isSport(paramsList.get(0))) {
            return new SportAircraft(paramsList.get(1), paramsList.get(2), Double.parseDouble(paramsList.get(3)), Double.parseDouble(paramsList.get(4)), Double.parseDouble(paramsList.get(5)), Integer.parseInt(paramsList.get(6)), Integer.parseInt(paramsList.get(7)), Double.parseDouble(paramsList.get(8)), paramsList.get(9), Integer.parseInt(paramsList.get(10)));
        }
        else if(isPassenger(paramsList.get(0))) {
            return new PassengerAircraft(paramsList.get(1), paramsList.get(2), Double.parseDouble(paramsList.get(3)), Double.parseDouble(paramsList.get(4)), Double.parseDouble(paramsList.get(5)), Integer.parseInt(paramsList.get(6)), Integer.parseInt(paramsList.get(7)), Double.parseDouble(paramsList.get(8)), Integer.parseInt(paramsList.get(9)));
        }
        else
        {
            return new TransportAircraft(paramsList.get(1), paramsList.get(2), Double.parseDouble(paramsList.get(3)), Double.parseDouble(paramsList.get(4)), Double.parseDouble(paramsList.get(5)), Integer.parseInt(paramsList.get(6)), Integer.parseInt(paramsList.get(7)), Double.parseDouble(paramsList.get(8)), Double.parseDouble(paramsList.get(9)));
        }
    }
}