package inifromfile;

import data.AbstractAircraft;

import java.util.List;

import static utils.Common.isMilitary;
import static utils.Common.stringToParams;

/**
 * Created by Andoliny on 18.10.2015.
 */
public class ConstructorChooser implements InitFromFile {

    @Override
    public AbstractAircraft initAircraft(String s) {
        return  chooseConstructor(stringToParams(s));
    }


    private AbstractAircraft chooseConstructor(List<String> params){
        if(isMilitary(params) == true){
            return new InitMilitary().createAircraft(params);
        }
        else
            return new InitCivil().createAircraft(params);
    }



}
