package utils;

import data.AbstractAircraft;

/**
 * Created by SGurin on 09.10.2015.
 */
public enum FilterOrder {

    MODEL_NAME {
        @Override
        public boolean check(AbstractAircraft o, Object value) {
            return o.getModelName().equals(value);
        }

        @Override
        public boolean inRange(AbstractAircraft o, Number from, Number to) {
            return false;
        }
    },
    REGISTRY_NUMBER {
        @Override
        public boolean check(AbstractAircraft o, Object value) {
            return o.getRegistryNumber().equals(value);
        }

        @Override
        public boolean inRange(AbstractAircraft o, Number from, Number to) {
            return false;
        }
    },
    SPEED_FLIGHT {
        @Override
        public boolean check(AbstractAircraft o, Object value) {
            return o.getSpeedFlight() == (double) value;
        }

        public boolean inRange(AbstractAircraft o, Number from, Number to) {
            return o.getSpeedFlight() > from.doubleValue() && o.getSpeedFlight() < to.doubleValue();
        }
    },
    HEIGHT_FLIGHT {
        @Override
        public boolean check(AbstractAircraft o, Object value) {
            return o.getHeightFlight() == (double) value;
        }

        @Override
        public boolean inRange(AbstractAircraft o, Number from, Number to) {
            return o.getHeightFlight() > from.doubleValue() && o.getHeightFlight() < to.doubleValue();
        }
    },
    CONSUMPTION_FUEL {
        @Override
        public boolean check(AbstractAircraft o, Object value) {
            return o.getConsumptionFuel() == (double) value;
        }

        @Override
        public boolean inRange(AbstractAircraft o, Number from, Number to) {
            return o.getConsumptionFuel() > from.doubleValue() && o.getConsumptionFuel() < to.doubleValue();
        }
    },
    HUMAN_CAPACITY {
        @Override
        public boolean check(AbstractAircraft o, Object value) {
            return o.getHumanCapacity() == (double) value;
        }

        @Override
        public boolean inRange(AbstractAircraft o, Number from, Number to) {
            return o.getHumanCapacity() > from.doubleValue() && o.getHumanCapacity() < to.doubleValue();
        }
    },
    STAFF_QUANTITY {
        @Override
        public boolean check(AbstractAircraft o, Object value) {
            return o.getStaffQuantity() == (double) value;
        }

        @Override
        public boolean inRange(AbstractAircraft o, Number from, Number to) {
            return o.getStaffQuantity() > from.doubleValue() && o.getStaffQuantity() < to.doubleValue();
        }
    },
    DISTANCE_FLIGHT {
        @Override
        public boolean check(AbstractAircraft o, Object value) {
            return o.getDistanceFlight() == (double) value;
        }

        @Override
        public boolean inRange(AbstractAircraft o, Number from, Number to) {
            return o.getDistanceFlight() > from.doubleValue() && o.getDistanceFlight() < to.doubleValue();
        }
    };

    public abstract boolean check(AbstractAircraft o, Object value);

    public abstract boolean inRange(AbstractAircraft o, Number from, Number to);

    private FilterOrder() {
    }
}


