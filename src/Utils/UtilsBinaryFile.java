package utils;

import data.AbstractAircraft;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static utils.Const.FILE_IN;
import static utils.Const.FILE_OUT;

/**
 * Created by SGurin on 19.10.2015.
 */
public class UtilsBinaryFile {
    public static List<AbstractAircraft> readBinaryFileIntoList() {
        ObjectInputStream objectInputStream = null;
        List<AbstractAircraft> list = new ArrayList<AbstractAircraft>();
        try {
            FileInputStream inputStream = new FileInputStream(FILE_IN);
            objectInputStream = new ObjectInputStream(inputStream);
            while (inputStream.available() > 0) {
                list.add((AbstractAircraft) objectInputStream.readObject());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeInputStream(objectInputStream);
            return list;
        }
    }

    public static void closeInputStream(ObjectInputStream objectInputStream) {
        if (objectInputStream != null)
            try {
                objectInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    public static void writeListIntoBinaryFile(List<AbstractAircraft> list) {
        ObjectOutputStream objectOutputStream = null;
        try {
            FileOutputStream outputStream = new FileOutputStream(FILE_OUT);
            objectOutputStream = new ObjectOutputStream(outputStream);
            for (AbstractAircraft abstractAircraft : list) {
                objectOutputStream.writeObject(abstractAircraft);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeOutputStream(objectOutputStream);
        }
    }

    public static void closeOutputStream(ObjectOutputStream objectOutputStream) {
        if (objectOutputStream != null)
            try {
                objectOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

}

