package utils;

/**
 * Created by Gurin S. on 03.10.2015.
 */

public final  class Const {

    public static final String DIR = "./src/attachment/AircraftsInfo.txt";
    public static final String FILE_OUT = "./src/attachment/data.out";
    public static final String FILE_IN = FILE_OUT;

    public static final String DEFAULT_REGISTRY_NUMBER = "-";
    public static final double DEFAULT_SPEED_FLIGHT = 200.;
    public static final double DEFAULT_HEIGHT_FLIGHT = 2.5;
    public static final double DEFAULT_CONSUMPTION_FUEL = 120;
    public static final int DEFAULT_HUMAN_CAPACITY = 1;
    public static final int DEFAULT_STAFF_QUANTITY = 1;
    public static final double DEFAULT_DISTANCE_FLIGHT = 1000;

    public static final int MAX_HUMAN_WEIGHT = 100;
    public static final int MAX_WEIGHT_UNIT_WEAPON = 100;

    private Const() {
    }
}
