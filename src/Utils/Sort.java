package utils;

import data.AbstractAircraft;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static utils.SortOrder.combineSortOrders;

/**
 * Created by SGurin on 20.10.2015.
 */
public class Sort {
    public static List<AbstractAircraft> aircraftsSorted(List<AbstractAircraft> list, SortOrder... characteristics) {
        Collections.sort(list, combineSortOrders( characteristics));
        return list;
    }
}
