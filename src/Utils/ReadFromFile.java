package utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andoliny on 17.10.2015.
 */
public class ReadFromFile {
    public static List readFile(String fileName) {
        List list = new ArrayList();
        BufferedReader reader = null;
        try{
            exists(fileName);
            reader = new BufferedReader(new FileReader(fileName));
            String s;
            while ((s = reader.readLine()) != null) {
                list.add(s);
            }
        }
        catch (FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            closeReader(reader);
        }

        return list;
    }

    public static void closeReader(BufferedReader reader) {
        if (reader != null) {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void exists(String fileName) throws FileNotFoundException {
        File file = new File(fileName);
        if (!file.exists()){
            throw new FileNotFoundException(file.getName());
        }
    }
}
