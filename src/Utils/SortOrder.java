package utils;

import data.AbstractAircraft;

import java.util.Comparator;

/**
 * Created by SGurin on 23.10.2015.
 */

public abstract class SortOrder implements Comparator<AbstractAircraft> {
    public static final SortOrder MODEL_NAME = new SortOrder() {
        @Override
          public int compare(AbstractAircraft o1, AbstractAircraft o2) {
            return o1.getModelName().compareTo(o2.getModelName());
        }
    };

    public static final SortOrder REGISTRY_NUMBER = new SortOrder() {
        @Override
        public int compare(AbstractAircraft o1, AbstractAircraft o2) {
            return o1.getRegistryNumber().compareTo(o2.getRegistryNumber());
        }
    };

    public static final SortOrder SPEED_FLIGHT = new SortOrder() {
        @Override
        public int compare(AbstractAircraft o1, AbstractAircraft o2) {
            return Double.compare(o1.getSpeedFlight(), o2.getSpeedFlight());
        }
    };

    public static final SortOrder HEIGHT_FLIGHT = new SortOrder() {
        @Override
        public int compare(AbstractAircraft o1, AbstractAircraft o2) {
            return Double.compare(o1.getHeightFlight(), o2.getHeightFlight());
        }

    };

    public static final SortOrder CONSUMPTION_FUEL = new SortOrder() {
        @Override
        public int compare(AbstractAircraft o1, AbstractAircraft o2) {
            return Double.compare(o1.getHeightFlight(), o2.getHeightFlight());
        }
    };

    public static final  SortOrder HUMAN_CAPACITY = new SortOrder() {
        @Override
        public int compare(AbstractAircraft o1, AbstractAircraft o2) {
            return Integer.compare(o1.getHumanCapacity(), o2.getHumanCapacity());
        }
    };

    public static final SortOrder STAFF_QUANTITY = new SortOrder() {
        @Override
        public int compare(AbstractAircraft o1, AbstractAircraft o2) {
            return Double.compare(o1.getStaffQuantity(), o2.getStaffQuantity());
        }
    };

    public static final SortOrder DISTANCE_FLIGHT = new SortOrder() {
        @Override
        public int compare(AbstractAircraft o1, AbstractAircraft o2) {
            return Double.compare(o1.getDistanceFlight(), o2.getDistanceFlight());
        }
    };

    public static SortOrder invertOrder(final SortOrder toInvert) {
        return new SortOrder() {
            public int compare(AbstractAircraft o1, AbstractAircraft o2) {
                return toInvert.compare(o2, o1);
            }
        };
    }

    public static Comparator<AbstractAircraft> combineSortOrders(final SortOrder... multipleSortOrders) {
        return new Comparator<AbstractAircraft>() {
            public int compare(AbstractAircraft o1, AbstractAircraft o2) {
                for (SortOrder personComparator : multipleSortOrders) {
                    int result = personComparator.compare(o1, o2);
                    if (result != 0) {
                        return result;
                    }
                }
                return 0;
            }
        };
    }

}
