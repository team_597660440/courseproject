package utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Gurin S. on 18.10.2015.
 */
public class FilterCriterions {

    private Map<FilterOrder, List<Object>> filterMainCriterions = new LinkedHashMap<>();
    private Map<FilterOrder, Number[]> filterRangeCriterions = new LinkedHashMap<>();


    public FilterCriterions addRange(FilterOrder characteristics, Number from, Number to) {
        Number[] array = {from,to};
        filterRangeCriterions.put(characteristics, array);
        return this;
    }

    public <T> FilterCriterions add(FilterOrder characteristics, T... value) {
        List<Object> list = formMainList(value);
        filterMainCriterions.put(characteristics, list);
        return this;
    }

    private <T> List<Object> formMainList(T... value) {
        List<Object> list = new ArrayList<>();
        for (Object val : value) {
            list.add(val);
        }
        return list;
    }

    public Map<FilterOrder, List<Object>> getFilterMainCriterions() {
        return filterMainCriterions;
    }

    public Map<FilterOrder,Number[]> getFilterRangeCriterions() {
        return filterRangeCriterions;
    }


    public void printCriterions() {
        System.out.println(filterMainCriterions);
        System.out.println(filterRangeCriterions);
    }
}
