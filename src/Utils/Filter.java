package utils;

import data.AbstractAircraft;

import java.util.*;

/**
 * Created by SGurin on 20.10.2015.
 */
public class Filter {
    public static List<AbstractAircraft> aircraftsFiltered(List<AbstractAircraft> abstractAircraftList, FilterCriterions filterCriterions) {
        List<AbstractAircraft> resultAircrafts = new ArrayList<>(abstractAircraftList);

        filterList(abstractAircraftList, filterCriterions, resultAircrafts);

        return resultAircrafts;
    }

    private static void filterList(List<AbstractAircraft> abstractAircraftList, FilterCriterions filterCriterions, List<AbstractAircraft> resultAircrafts) {
        filterByRange(abstractAircraftList, filterCriterions, resultAircrafts);

        filterByOtherCriterions(abstractAircraftList, filterCriterions, resultAircrafts);
    }

    private static void filterByRange(List<AbstractAircraft> abstractAircraftList, FilterCriterions filterCriterions, List<AbstractAircraft> resultAircrafts) {
        for (AbstractAircraft aircraft : abstractAircraftList) {
            for (Map.Entry<FilterOrder, Number[]> listCriterion : filterCriterions.getFilterRangeCriterions().entrySet()) {
                Number[] valueCriterion = listCriterion.getValue();
                if (!listCriterion.getKey().inRange(aircraft, valueCriterion[0], valueCriterion[1]))
                    resultAircrafts.remove(aircraft);
    }
}
    }

    private static void filterByOtherCriterions(List<AbstractAircraft> abstractAircraftList, FilterCriterions filterCriterions, List<AbstractAircraft> resultAircrafts) {
        boolean flag = true;
        for (AbstractAircraft aircraft : abstractAircraftList) {
            for (Map.Entry<FilterOrder, List<Object>> listCriterion : filterCriterions.getFilterMainCriterions().entrySet()) {
                for (Object valueCriterion : listCriterion.getValue()) {
                    flag = listCriterion.getKey().check(aircraft, valueCriterion);
                    if (flag)
                        break;
                }
                if (!flag)
                    resultAircrafts.remove(aircraft);
            }
        }
    }

}





