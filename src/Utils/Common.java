package utils;

import com.sun.org.apache.xpath.internal.SourceTree;
import data.AbstractAircraft;

import javax.xml.ws.EndpointReference;
import java.io.*;
import java.util.*;

import static utils.Const.*;

/**
 * Created by SGurin on 09.10.2015.
 * Created by Andoliny on 17.10.2015.
 */
public class Common {

    public static void checkString(String str) {
        if (str.equals("") || str == null) {
            System.out.println("Parameter String is empty");
            System.exit(0);
        }
    }

    public static void checkInt(int n) {
        if (n < 0)
            System.out.println("Parameter Int should be positive");
    }

    public static void checkDouble(double n) {
        if (n < 0.)
            System.out.println("Parameter Double should be positive");
    }

    public static boolean isDouble(String input) {
        return checkDouble(input);
    }

    public static boolean isInt(String input) {
        return checkInt(input);
    }

    private static boolean checkDouble(String input) {
        try {
            Double.parseDouble(input);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static boolean checkInt(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static double calculateWeightCapacity(List<AbstractAircraft> aircrafts) {
        double weightCapacity = 0;
        for (AbstractAircraft aircraft : aircrafts) {
            weightCapacity = weightCapacity + aircraft.calcWeightCapacity();
        }
        return weightCapacity;
    }

    public static int calculateHumanCapacity(List<AbstractAircraft> aircrafts) {
        int humanCapacity = 0;
        for (AbstractAircraft aircraft : aircrafts) {
            humanCapacity = humanCapacity + aircraft.getHumanCapacity();
        }
        return humanCapacity;
    }

    public static List<AbstractAircraft> consumptionInRange(double minVal, double maxVal, ArrayList<AbstractAircraft> aircraft) {
        List<AbstractAircraft> result = new ArrayList<AbstractAircraft>();
        for (AbstractAircraft aircrafts : aircraft) {
            double fuel = aircrafts.getConsumptionFuel();
            if (fuel >= minVal && fuel <= maxVal) {
                result.add(aircrafts);
            }
        }
        return result;
    }

    public static void printList(List<AbstractAircraft> list) {
        for (AbstractAircraft abstractAircraft : list) {
            System.out.println(abstractAircraft);
        }
    }

    public static List<String> stringToParams(String inputString) {
        List<String> paramsList = new ArrayList();
        String[] parts = inputString.split(";");
        for (String element : parts) {
            paramsList.add(element.trim());
        }
        return paramsList;
    }

    public static boolean isMilitary(List params) {
        return (params.get(0).equals("Fighter") || params.get(0).equals("Bomber") || params.get(0).equals("AttackPlane"));
    }

    public static boolean isBomber(String param) {
        return (param.equals("Bomber"));
    }

    public static boolean isSport(String param) {
        return (param.equals("SportAircraft"));
    }

    public static boolean isAttackPlane(String param) {
        return (param.equals("AttackPlane"));
    }

    public static boolean isPassenger(String param) {
        return (param.equals("PassengerAircraft"));
    }

    public static boolean isTransport(String param) {
        return (param.equals("TransportAircraft"));
    }

    public static boolean isFighter(String param) {
        return (param.equals("Fighter"));
    }

}
