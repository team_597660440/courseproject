package run;

import data.AirCompany;
import inifromfile.ConstructorChooser;
import inifromfile.InitFromFile;
import utils.FilterCriterions;
import utils.FilterOrder;
import utils.SortOrder;

import java.util.List;

import static utils.Common.printList;
import static utils.Const.DIR;
import static utils.Filter.aircraftsFiltered;
import static utils.ReadFromFile.readFile;
import static utils.Sort.aircraftsSorted;
import static utils.SortOrder.invertOrder;

/**
 * Created by Gurin S. on 03.10.2015.
 */

public class AirlineRunner {

    public static void main(String[] args) {

        AirCompany airCompany = new AirCompany();

        InitFromFile initiator = new ConstructorChooser();

        List<String> list = readFile(DIR);

        for (int i = 0; i < list.size(); i++) {
            airCompany.addAircraft(initiator.initAircraft(list.get(i)));
        }

        aircraftsSorted(airCompany.getListAircrafts(), SortOrder.CONSUMPTION_FUEL, SortOrder.MODEL_NAME);

        aircraftsSorted(airCompany.getListAircrafts(), invertOrder(SortOrder.MODEL_NAME), SortOrder.DISTANCE_FLIGHT);

        printList(airCompany.getListAircrafts());

        FilterCriterions filterCriterions = new FilterCriterions();

        filterCriterions.add(FilterOrder.MODEL_NAME, "F-16", "F-17").add(FilterOrder.REGISTRY_NUMBER, "02-5698").addRange(FilterOrder.DISTANCE_FLIGHT,1000,5000);

        printList(aircraftsFiltered(airCompany.getListAircrafts(), filterCriterions));

    }
}









