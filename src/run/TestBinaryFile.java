package run;

import data.*;
import utils.FilterOrder;
import utils.FilterCriterions;
import utils.SortOrder;

import java.util.List;

import static utils.Common.*;
import static utils.Filter.aircraftsFiltered;
import static utils.Sort.aircraftsSorted;
import static utils.SortOrder.invertOrder;
import static utils.UtilsBinaryFile.readBinaryFileIntoList;
import static utils.UtilsBinaryFile.writeListIntoBinaryFile;

/**
 * Created by Gurin S. on 18.10.2015.
 */
public class TestBinaryFile {
    public static void main(String[] args) {
        //ReadFile
        List<AbstractAircraft> list = readBinaryFileIntoList();
        printList(list);

        MilitaryAircraft fighter1 = new Fighter("F-16", "02-5698", 8, Boolean.TRUE);
        fighter1.setConsumptionFuel(200);
        fighter1.setWeaponsQuantity(8);
        fighter1.setDistanceFlight(700);
        fighter1.setHumanCapacity(3);
        fighter1.setSpeedFlight(1400);

        MilitaryAircraft fighter2 = new Fighter("F-16", "02-5699", 9, Boolean.TRUE);
        fighter2.setSpeedFlight(1900);

        AbstractAircraft passengerAircraft = new PassengerAircraft("AN-158", 150);
        passengerAircraft.setDistanceFlight(700);
        passengerAircraft.setHumanCapacity(6);

        AbstractAircraft sportAircraft = new SportAircraft("SU-26", "10G", 5);
        sportAircraft.setConsumptionFuel(150.);
        sportAircraft.setDistanceFlight(1700);
        sportAircraft.setHumanCapacity(9);
        sportAircraft.setSpeedFlight(2500);

        AirCompany airCompany = new AirCompany(list);
        airCompany.addAircraft(fighter1);
        airCompany.addAircraft(fighter2);
        airCompany.addAircraft(passengerAircraft);
        airCompany.addAircraft(sportAircraft);
        printList(airCompany.getListAircrafts());

        System.out.println("Sort");
        printList(aircraftsSorted(airCompany.getListAircrafts(), SortOrder.MODEL_NAME, SortOrder.DISTANCE_FLIGHT));

        printList(aircraftsSorted(airCompany.getListAircrafts(), invertOrder(SortOrder.MODEL_NAME), SortOrder.DISTANCE_FLIGHT));

        System.out.println("Filter");
        FilterCriterions filterCriterions = new FilterCriterions();

        filterCriterions.add(FilterOrder.MODEL_NAME, "F-16", "F-17").add(FilterOrder.REGISTRY_NUMBER, "02-5698").addRange(FilterOrder.SPEED_FLIGHT, 1300, 1800);

        printList(aircraftsFiltered(airCompany.getListAircrafts(), filterCriterions));

        System.out.println("Utils");
        System.out.println("calculateWeightCapacity: " + calculateWeightCapacity(airCompany.getListAircrafts()));

        System.out.println("consumptionInRange: " + consumptionInRange(0., 160., airCompany.getListAircrafts()));

        System.out.println("getConsumptionFuel: " + sportAircraft.getConsumptionFuel());

        System.out.println("WriteFile");
        writeListIntoBinaryFile(airCompany.getListAircrafts());
    }
}
