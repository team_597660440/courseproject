package data;

import static utils.Const.*;

/**
 * Created by Gurin S. on 03.10.2015.
 */

public class TransportAircraft extends AbstractCivilAircraft {
    private double luggageCapacity;

    @Override
    public double calcWeightCapacity() {
        return (double)(getHumanCapacity()*MAX_HUMAN_WEIGHT + getLuggageCapacity());
    }

    public TransportAircraft() {
    }

    public TransportAircraft(String modelName, double luggageCapacity) {
        super(modelName, TimeInService.TRANSPORT_AIRCRAFT);
        setLuggageCapacity(luggageCapacity);
    }

    public TransportAircraft(String modelName, String registryNumber, double luggageCapacity) {
        super(modelName, registryNumber, TimeInService.TRANSPORT_AIRCRAFT);
        setLuggageCapacity(luggageCapacity);
    }

    public TransportAircraft(String modelName, String registryNumber, double speedFlight, double luggageCapacity) {
        super(modelName, registryNumber, speedFlight, TimeInService.TRANSPORT_AIRCRAFT);
        setLuggageCapacity(luggageCapacity);
    }

    public TransportAircraft(String modelName, String registryNumber, double speedFlight, double heightFlight, double consumptionFuel, int humanCapacity, int staffQuantity,  double distanceFlight, double luggageCapacity) {
        super(modelName, registryNumber, speedFlight, TimeInService.TRANSPORT_AIRCRAFT);
        setModelName(modelName);
        setRegistryNumber(registryNumber);
        setSpeedFlight(speedFlight);
        setHeightFlight(heightFlight);
        setConsumptionFuel(consumptionFuel);
        setHumanCapacity(humanCapacity);
        setStaffQuantity(staffQuantity);
        setDistanceFlight(distanceFlight);
        setLuggageCapacity(luggageCapacity);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TransportAircraft   {");
        sb.append(super.toString());
        sb.append(", luggageCapacity=");
        sb.append(getLuggageCapacity());
        sb.append('}');
        return sb.toString();
    }

    //  Getters and Setters

    public double getLuggageCapacity() {
        return luggageCapacity;
    }

    public void setLuggageCapacity(double luggageCapacity) {
        this.luggageCapacity = luggageCapacity;
    }
}
