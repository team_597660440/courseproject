
package data;

/**
 * Created by Gurin S. on 03.10.2015.
 */

public class Bomber extends MilitaryAircraft {
    private double radiusAction;

    public Bomber() {
    }

    public Bomber(String modelName, int weaponsQuantity, double radiusAction) {
        super(modelName, weaponsQuantity, TypeOfAmmo.CLASS_B);
        setRadiusAction(radiusAction);
    }

    public Bomber(String modelName, String registryNumber, int weaponsQuantity, double radiusAction) {
        super(modelName, registryNumber, weaponsQuantity, TypeOfAmmo.CLASS_B);
        setRadiusAction(radiusAction);
    }

    public Bomber(String modelName, String registryNumber, double speedFlight, double heightFlight, double consumptionFuel, int humanCapacity, int staffQuantity,  double distanceFlight, int weaponsQuantity, double radiusAction) {
        super(modelName, registryNumber, weaponsQuantity, TypeOfAmmo.CLASS_B);
        setModelName(modelName);
        setRegistryNumber(registryNumber);
        setSpeedFlight(speedFlight);
        setHeightFlight(heightFlight);
        setConsumptionFuel(consumptionFuel);
        setHumanCapacity(humanCapacity);
        setStaffQuantity(staffQuantity);
        setDistanceFlight(distanceFlight);
        setRadiusAction(radiusAction);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Bomber              {");
        sb.append(super.toString());
        sb.append(", radiusAction=");
        sb.append(getRadiusAction());
        sb.append('}');
        return sb.toString();
    }

    //  Getters and Setters

    public double getRadiusAction() {
        return radiusAction;
    }

    public void setRadiusAction(double radiusAction) {
        this.radiusAction = radiusAction;
    }
}
