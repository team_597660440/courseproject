package data;

/**
 * Created by Gurin S. on 03.10.2015.
 */

public interface Aircraft {
    public double calcWeightCapacity();
}
