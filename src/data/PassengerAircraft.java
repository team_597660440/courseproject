package data;

import static utils.Const.*;

/**
 * Created by Gurin S. on 03.10.2015.
 */

public class PassengerAircraft extends AbstractCivilAircraft {
    private int passengerSeatsQuantity;

    @Override
    public double calcWeightCapacity() {
        return (double) (getPassengerSeatsQuantity()*MAX_HUMAN_WEIGHT);
    }

    public PassengerAircraft() {}

    public PassengerAircraft(String modelName, int passengerSeatsQuantity) {
        super(modelName, TimeInService.PASSENGER_AIRCRAFT);
        setPassengerSeatsQuantity(passengerSeatsQuantity);
    }

    public PassengerAircraft(String modelName, String registryNumber, int passengerSeatsQuantity) {
        super(modelName, registryNumber, TimeInService.PASSENGER_AIRCRAFT);
        setPassengerSeatsQuantity(passengerSeatsQuantity);
    }

    public PassengerAircraft(String modelName, String registryNumber, double speedFlight, int passengerSeatsQuantity) {
        super(modelName, registryNumber, speedFlight, TimeInService.PASSENGER_AIRCRAFT);
        setPassengerSeatsQuantity(passengerSeatsQuantity);
    }

    public PassengerAircraft(String modelName, String registryNumber, double speedFlight, double heightFlight, double consumptionFuel, int humanCapacity, int staffQuantity,  double distanceFlight, int passengerSeatsQuantity) {
        super(modelName, registryNumber,speedFlight, TimeInService.PASSENGER_AIRCRAFT);
        setModelName(modelName);
        setRegistryNumber(registryNumber);
        setSpeedFlight(speedFlight);
        setHeightFlight(heightFlight);
        setConsumptionFuel(consumptionFuel);
        setHumanCapacity(humanCapacity);
        setStaffQuantity(staffQuantity);
        setDistanceFlight(distanceFlight);
        setPassengerSeatsQuantity(passengerSeatsQuantity);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PassengerAircraft   {");
        sb.append(super.toString());
        sb.append(", seatsQuantity=");
        sb.append(getPassengerSeatsQuantity());
        sb.append('}');
        return sb.toString();
    }

    //  Getters and Setters

    public int getPassengerSeatsQuantity() {
        return passengerSeatsQuantity;
    }

    public void setPassengerSeatsQuantity(int passengerSeatsQuantity) {
        this.passengerSeatsQuantity = passengerSeatsQuantity;
    }
}
