package data;

import utils.Common;

import static utils.Const.MAX_HUMAN_WEIGHT;


/**
 * Created by Gurin S. on 03.10.2015.
 */

public class MilitaryAircraft extends AbstractAircraft {
    private int weaponsQuantity;
    private TypeOfAmmo rocket;
    private int rocketWeight;

    public MilitaryAircraft() {
    }

    public MilitaryAircraft(String modelName, int weaponsQuantity, TypeOfAmmo rocket) {
        super(modelName);
        setWeaponsQuantity(weaponsQuantity);
        setRocket(rocket);
    }

    public MilitaryAircraft(String modelName, String registryNumber, int weaponsQuantity, TypeOfAmmo rocket) {
        super(modelName, registryNumber);
        setWeaponsQuantity(weaponsQuantity);
        setRocket(rocket);
    }

    @Override
    public double calcWeightCapacity() {
        return (double) (getHumanCapacity() * MAX_HUMAN_WEIGHT + getWeaponsQuantity() * getRocketWeight());
    }

    //  Getters and Setters

    public int getWeaponsQuantity() {
        return weaponsQuantity;
    }

    public void setWeaponsQuantity(int weaponsQuantity) {
        Common.checkInt(weaponsQuantity);
        this.weaponsQuantity = weaponsQuantity;
    }

    public TypeOfAmmo getRocket() {
        return rocket;
    }

    public int getRocketWeight() {
        return rocket.getWeight();
    }

    private void setRocket(TypeOfAmmo rocket) {
        this.rocket = rocket;
    }
}
