package data;

/**
 * Created by Gurin S. on 03.10.2015.
 */

public abstract class AbstractCivilAircraft extends AbstractAircraft {

    private TimeInService lifeTime;

    public AbstractCivilAircraft() {
    }

    public AbstractCivilAircraft(String modelName, TimeInService lifeTime) {
        super(modelName);
        setLifeTime(lifeTime);
    }

    public AbstractCivilAircraft(String modelName, String registryNumber, TimeInService lifeTime) {
        super(modelName, registryNumber);
        setLifeTime(lifeTime);
    }

    public AbstractCivilAircraft(String modelName, String registryNumber, double speedFlight, TimeInService lifeTime) {
        super(modelName, registryNumber, speedFlight);
        setLifeTime(lifeTime);
    }

    public int runningTime() {
        return lifeTime.getYearsQuantity();
    }

    @Override
    public abstract double calcWeightCapacity();

    //  Getters and Setters

    public TimeInService getLifeTime() {
        return lifeTime;
    }

    public void setLifeTime(TimeInService lifeTime) {
        this.lifeTime = lifeTime;
    }
}
