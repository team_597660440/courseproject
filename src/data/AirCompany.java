package data;

import java.util.*;

/*
* Авиакомпания. Определить иерархию самолетов. Создать авиакомпанию.
* Посчитать общую вместимость и грузоподъемность. Провести сортировку
* самолетов компании по дальности полета. Найти самолет в компании, со-
* ответствующий заданному диапазону параметров потребления горючего.
*/

public class AirCompany {
    private ArrayList<AbstractAircraft> aircrafts = new ArrayList<AbstractAircraft>();

    public void addAircraft(AbstractAircraft aircraft) {
        aircrafts.add(aircraft);
    }
    public AirCompany(List<AbstractAircraft> list) {
        aircrafts.addAll(list);
    }

    public AirCompany() {
    }

    public ArrayList<AbstractAircraft> getListAircrafts() {
       return aircrafts;
    }

}
