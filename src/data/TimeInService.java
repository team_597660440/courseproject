package data;

/**
 * Created by SGurin on 27.10.2015.
 */
public enum TimeInService {
    PASSENGER_AIRCRAFT(15),
    TRANSPORT_AIRCRAFT(20),
    SPORT_AIRCRAFT(20);

    private TimeInService(int yearsQuantity) {
        this.yearsQuantity = yearsQuantity;
    }

    private int yearsQuantity;

    public int getYearsQuantity() {
        return yearsQuantity;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TimeInService{");
        sb.append("yearsQuantity=").append(yearsQuantity);
        sb.append('}');
        return sb.toString();
    }
}
