package data;

/**
 * Created by Andoliny on 11.10.2015.
 */
public enum TypeOfAmmo {
    CLASS_A(100), CLASS_B(135), CLASS_C(150);

    TypeOfAmmo(int weight) {
        this.weight = weight;
    }

    private final int weight;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TypeOfAmmo{");
        sb.append("weight=").append(weight);
        sb.append('}');
        return sb.toString();
    }

    public int getWeight() {
        return weight;
    }
}
