package data;

import utils.*;

import java.io.Serializable;

import static utils.Const.*;

/**
 * Created by Gurin S. on 03.10.2015.
 */

public abstract class AbstractAircraft implements Aircraft, Serializable {
    private static final long serialVersionUID = -5356356834126245505L;

    private String modelName;
    private String registryNumber;
    private double speedFlight;
    private double heightFlight;
    private double consumptionFuel;
    private int humanCapacity;
    private int staffQuantity;
    private double distanceFlight;

    public abstract double calcWeightCapacity();

    protected AbstractAircraft() {
    }

    protected AbstractAircraft(String modelName) {
        this(modelName, DEFAULT_REGISTRY_NUMBER);
    }

    protected AbstractAircraft(String modelName, String registryNumber) {
        this(modelName, registryNumber, DEFAULT_SPEED_FLIGHT);
    }

    protected AbstractAircraft(String modelName, String registryNumber, double speedFlight) {
        this(modelName, registryNumber, speedFlight, DEFAULT_HEIGHT_FLIGHT);
    }

    protected AbstractAircraft(String modelName, String registryNumber, double speedFlight, double heightFlight) {
        this(modelName, registryNumber, speedFlight, heightFlight, DEFAULT_CONSUMPTION_FUEL);
    }

    protected AbstractAircraft(String modelName, String registryNumber, double speedFlight, double heightFlight, double consumptionFuel) {
        this(modelName, registryNumber, speedFlight, heightFlight, consumptionFuel, DEFAULT_HUMAN_CAPACITY);
    }

    protected AbstractAircraft(String modelName, String registryNumber, double speedFlight, double heightFlight, double consumptionFuel, int humanCapacity) {
        this(modelName, registryNumber, speedFlight, heightFlight, consumptionFuel, humanCapacity, DEFAULT_STAFF_QUANTITY);
    }

    protected AbstractAircraft(String modelName, String registryNumber, double speedFlight, double heightFlight, double consumptionFuel, int humanCapacity, int staffQuantity) {
        this(modelName, registryNumber, speedFlight, heightFlight, consumptionFuel, humanCapacity, staffQuantity, DEFAULT_DISTANCE_FLIGHT);
    }

    protected AbstractAircraft(String modelName, String registryNumber, double speedFlight, double heightFlight, double consumptionFuel, int humanCapacity, int staffQuantity,  double distanceFlight) {
        setModelName(modelName);
        setRegistryNumber(registryNumber);
        setSpeedFlight(speedFlight);
        setHeightFlight(heightFlight);
        setConsumptionFuel(consumptionFuel);
        setHumanCapacity(humanCapacity);
        setStaffQuantity(staffQuantity);
        setDistanceFlight(distanceFlight);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("");
        sb.append("modelName='").append(modelName).append('\'');
        sb.append(", registryNumber='").append(registryNumber).append('\'');
        sb.append(", speedFlight=").append(speedFlight);
        sb.append(", heightFlight=").append(heightFlight);
        sb.append(", consumptionFuel=").append(consumptionFuel);
        sb.append(", humanCapacity=").append(humanCapacity);
        sb.append(", staffQuantity=").append(staffQuantity);
        sb.append(", distanceFlight=").append(distanceFlight);
        return sb.toString();
    }

    //  Getters and Setters

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        Common.checkString(modelName);
        this.modelName = modelName;
    }

    public String getRegistryNumber() {
        return registryNumber;
    }

    public void setRegistryNumber(String registryNumber) {
        Common.checkString(registryNumber);
        this.registryNumber = registryNumber;
    }

    public double getSpeedFlight() {
        return speedFlight;
    }

    public void setSpeedFlight(double speedFlight) {
        Common.checkDouble(speedFlight);
        this.speedFlight = speedFlight;
    }

    public double getHeightFlight() {
        return heightFlight;
    }

    public void setHeightFlight(double heightFlight) {
        Common.checkDouble(heightFlight);
        this.heightFlight = heightFlight;
    }

    public double getConsumptionFuel() {
        return consumptionFuel;
    }

    public void setConsumptionFuel(double consumptionFuel) {
        Common.checkDouble(consumptionFuel);
        this.consumptionFuel = consumptionFuel;
    }

    public int getHumanCapacity() {
        return humanCapacity;
    }

    public void setHumanCapacity(int humanCapacity) {
        Common.checkInt(humanCapacity);
        this.humanCapacity = humanCapacity;
    }

    public int getStaffQuantity() {
        return staffQuantity;
    }

    public void setStaffQuantity(int staffQuantity) {
        Common.checkInt(staffQuantity);
        this.staffQuantity = staffQuantity;
    }

    public double getDistanceFlight() {
        return distanceFlight;
    }

    public void setDistanceFlight(double distanceFlight) {
        Common.checkDouble(distanceFlight);
        this.distanceFlight = distanceFlight;
    }
}
