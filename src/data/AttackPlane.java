package data;

/**
 * Created by Gurin S. on 03.10.2015.
 */

public class AttackPlane extends MilitaryAircraft {
    private boolean haveNuclearWeapons;

    public AttackPlane() {
    }

    public AttackPlane(String modelName, int weaponsQuantity, boolean haveNuclearWeapons) {
        super(modelName, weaponsQuantity, TypeOfAmmo.CLASS_C);
        setHaveNuclearWeapons(haveNuclearWeapons);
    }

    public AttackPlane(String modelName, String registryNumber, int weaponsQuantity, boolean haveNuclearWeapons) {
        super(modelName, registryNumber, weaponsQuantity, TypeOfAmmo.CLASS_C);
        setHaveNuclearWeapons(haveNuclearWeapons);
    }

    public AttackPlane(String modelName, String registryNumber, double speedFlight, double heightFlight, double consumptionFuel, int humanCapacity, int staffQuantity,  double distanceFlight, int weaponsQuantity, boolean haveNuclearWeapons) {
        super(modelName, registryNumber, weaponsQuantity, TypeOfAmmo.CLASS_C);
        setModelName(modelName);
        setRegistryNumber(registryNumber);
        setSpeedFlight(speedFlight);
        setHeightFlight(heightFlight);
        setConsumptionFuel(consumptionFuel);
        setHumanCapacity(humanCapacity);
        setStaffQuantity(staffQuantity);
        setDistanceFlight(distanceFlight);
        setHaveNuclearWeapons(haveNuclearWeapons);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AttackPlane         {");
        sb.append(super.toString());
        sb.append(", haveNuclearWeapons=");
        sb.append(getHaveNuclearWeapons());
        sb.append('}');
        return sb.toString();
    }

    //  Getters and Setters

    public boolean getHaveNuclearWeapons() {
        return haveNuclearWeapons;
    }

    public void setHaveNuclearWeapons(boolean haveNuclearWeapons) {
        this.haveNuclearWeapons = haveNuclearWeapons;
    }
}
