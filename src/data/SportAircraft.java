package data;

import static utils.Const.*;


/**
 * Created by Gurin S. on 03.10.2015.
 */

public class SportAircraft extends AbstractCivilAircraft {
    private String permissibleOverload;
    private int category;

    @Override
    public double calcWeightCapacity() {
        return (double)(getHumanCapacity()*MAX_HUMAN_WEIGHT);
    }

    public SportAircraft() {
    }

    public SportAircraft(String modelName, String permissibleOverload, int category) {
        super(modelName, TimeInService.SPORT_AIRCRAFT);
        setPermissibleOverload(permissibleOverload);
        setCategory(category);
    }

    public SportAircraft(String modelName, String registryNumber, String permissibleOverload, int category) {
        super(modelName, registryNumber, TimeInService.SPORT_AIRCRAFT);
        setPermissibleOverload(permissibleOverload);
        setCategory(category);
    }

    public SportAircraft(String modelName, String registryNumber, double speedFlight, double heightFlight, double consumptionFuel, int humanCapacity, int staffQuantity,  double distanceFlight, String permissibleOverload, int category) {
        super(modelName, registryNumber, speedFlight, TimeInService.SPORT_AIRCRAFT);
        setModelName(modelName);
        setRegistryNumber(registryNumber);
        setSpeedFlight(speedFlight);
        setHeightFlight(heightFlight);
        setConsumptionFuel(consumptionFuel);
        setHumanCapacity(humanCapacity);
        setStaffQuantity(staffQuantity);
        setDistanceFlight(distanceFlight);
        setPermissibleOverload(permissibleOverload);
        setCategory(category);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SportAircraft       {");
        sb.append(super.toString());
        sb.append(", permissibleOverload=");
        sb.append(getPermissibleOverload());
        sb.append(", category=");
        sb.append(getCategory());
        sb.append('}');
        return sb.toString();
    }

    //  Getters and Setters

    public String getPermissibleOverload() {
        return permissibleOverload;
    }

    public void setPermissibleOverload(String permissibleOverload) {
        this.permissibleOverload = permissibleOverload;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }
}
