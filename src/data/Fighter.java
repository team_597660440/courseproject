
package data;

/**
 * Created by Gurin S. on 03.10.2015.
 */

public class Fighter extends MilitaryAircraft {
    private boolean invisibility;

    public Fighter() {
    }

    public Fighter(String modelName, int weaponsQuantity, boolean invisibility) {
        super(modelName, weaponsQuantity, TypeOfAmmo.CLASS_A);
        setInvisibility(invisibility);
    }

    public Fighter(String modelName, String registryNumber, int weaponsQuantity, boolean invisibility) {
        super(modelName, registryNumber, weaponsQuantity, TypeOfAmmo.CLASS_A);
        setInvisibility(invisibility);
    }

    public Fighter(String modelName, String registryNumber, double speedFlight, double heightFlight, double consumptionFuel, int humanCapacity, int staffQuantity,  double distanceFlight, int weaponsQuantity, boolean invisibility) {
        super(modelName, registryNumber, weaponsQuantity, TypeOfAmmo.CLASS_A);
        setModelName(modelName);
        setRegistryNumber(registryNumber);
        setSpeedFlight(speedFlight);
        setHeightFlight(heightFlight);
        setConsumptionFuel(consumptionFuel);
        setHumanCapacity(humanCapacity);
        setStaffQuantity(staffQuantity);
        setDistanceFlight(distanceFlight);
        setInvisibility(invisibility);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Fighter             {");
        sb.append(super.toString());
        sb.append(", invisibility=");
        sb.append(getInvisibility());
        sb.append(", weightCapacity=");
        sb.append(calcWeightCapacity());
        sb.append('}');
        return sb.toString();
    }

    //  Getters and Setters

    public boolean getInvisibility() {
        return invisibility;
    }

    public void setInvisibility(boolean invisibility) {
        this.invisibility = invisibility;
    }
}
